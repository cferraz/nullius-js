import { expect } from 'chai'

import {
  map,
  flatMap,
  flatMap1,
  flatten,
  flatten1
} from '~/src/prelude/list'

describe('Prelude', () => {

  describe('#map', () => {
    it('should map the list', () => {
      const input    = [1, 2, 3, 4]
          , mapped   = (input::map(it => it * 2))
          , expected = [2, 4, 6, 8]
      expect(mapped).to.be.deep.equal(expected)
    })
  })

  describe('#flatMap', () => {
    it('should map and flatten the list recursively', () => {
      const input    = [1, 2, 3, 4]
          , mapped   = (input::flatMap(it => [[it, it * 2]]))
          , expected = [1, 2, 2, 4, 3, 6, 4, 8]
      expect(mapped).to.be.deep.equal(expected)
    })
  })

  describe('#flatMap1', () => {
    it('should map and flatten the list one level', () => {
      const input    = [1, [2], [[3]], 4]
          , mapped   = (input::flatMap1(it => {
            if (it instanceof Array) {
              return [it]
            } else {
              return [it, it * 2]
            }
          }))
          , expected = [1, 2, [2], [[3]], 4, 8]
      expect(mapped).to.be.deep.equal(expected)
    })
  })


  describe('#flatten', () => {
    it('should flatten the list recursively', () => {
      const input     = [1, [2], [[3]], 4]
          , flattened = (input::flatten())
          , expected  = [1, 2, 3, 4]
      expect(flattened).to.be.deep.equal(expected)
    })
  })

  describe('#flatten1', () => {
    it('should flatten the list by one level', () => {
      const input     = [1, [2], [[3]], 4]
          , flattened = (input::flatten1())
          , expected  = [1, 2, [3], 4]
      expect(flattened).to.be.deep.equal(expected)
    })
  })
})
