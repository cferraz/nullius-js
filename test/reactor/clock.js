/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
  test@Reactor.Clock
  Tests the `Clock` procedure
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  This file is part of 'carrot-engine'.

  'carrot-engine' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  'carrot-engine' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with 'carrot-engine'. If not, see <http://www.gnu.org/licenses/>.
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*
import { expect } from 'chai'
import {default as Clock, currentTime} from '~/src/reactor/clock'

describe('Reactor', () => {
  describe('#Clock', () => {

    it('should not crash', (done) => {
      Clock(function clock(stop) { stop(); done() })
    })

    it('should not take more than 8ms to create', (done) => {
      const init = currentTime()

      Clock(function clock(stop, time) {
        const measure = time-init

        stop()

        if (measure > 8) {
          throw new Error('Took to long to init the clock. (${measure})')
        } else {
          done()
        }
      })
    })

    it('should be precise', (done) => {
      var loopCount = 0
        , init = currentTime()

      Clock(function clock(stop, time) {
        if (loopCount == 30) {
          const measure = Math.abs(((1/60)*1000)-((currentTime() - init)/30))

          stop()

          if (measure > 1) {
            throw new Error('Clock is inconsistent. (${measure})')
          } else {
            done()
          }

        }
        loopCount += 1
      }) // SmartClock
    })
  })
})
*/
