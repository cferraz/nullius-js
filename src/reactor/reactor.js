/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
  Reactor.Reactor
    Reactive programming procedure, managed state and easy testing
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  This file is part of 'carrot-engine'.

  'carrot-engine' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  'carrot-engine' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with 'carrot-engine'. If not, see <http://www.gnu.org/licenses/>.
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Module Definition
import {default as Clock, currentTime} from '~/src/reactor/clock'
import {default as Stream, watch, unwatch} from '~/src/reactor/stream'
import isDef from '~/src/prelude/type/is-def'

const Effects = []
    , SceneCache = {}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *\
  Main Procedure
\* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
export default function Reactor (sceneλ) {
  var state  = { startup: true }
    , scene = sceneλ(state)

  if (!scene) {
    console.warn('No initial scene found.')
    return
  }

  var world = initScene(scene)

  addEffect(state::scene.initialize(currentTime()))

  Clock(function reactorClock(stop) {
    const nextScene = sceneλ(state)

    if (nextScene) {
      if (nextScene != scene) {
        termScene(scene)
        world = initScene(nextScene)
        addEffect(state::scene.dispose(currentTime()))
        scene  = nextScene
        addEffect(state::scene.initialize(currentTime()))
      } else {
        addEffect(state::scene.update(currentTime()))
      }
    } else {
      termScene(scene)
      addEffect(state::scene.dispose(currentTime()))
      stop()
    }

    // side-effects
    if (typeof global.it != 'function') {
      const length = Effects.length
      for (let i=0; i < length; ++i) { world::Effects[i](state) }
    }
  })
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *\
  Schedules a Effect
\* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
function addEffect(val) {
  if (val && ('function' != typeof global.it)) {
    if (typeof val == 'function') {
      Effects.push(val)
    } else {
      const length = val.length
      for (let i=0; i < length; ++i) { Effects.push(val[i]) }
    }
  }
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *\
  Starts the Scene
\* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
function initScene(scene) {
  if (isDef(SceneCache[scene])) {
    return SceneCache[scene]
  }

  const data = {
    stream: Stream(),
    world: {}
  }

  data.childs = scene::scene.default(data.stream.api) || []

  addEffect(data.stream::watch())

  SceneCache[scene] = data

  return data.world
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *\
  Terminates the scene
\* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
function termScene(scene) {
  const data = SceneCache[scene]
  data.stream::unwatch()
  return data
}
