/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
  Reactor.Stream
    Reactive signal stream :)
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  This file is part of 'carrot-engine'.

  'carrot-engine' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  'carrot-engine' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with 'carrot-engine'. If not, see <http://www.gnu.org/licenses/>.
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// replace each with for loops later for performance
import each from '~/src/prelude/list/each'

// Signal list
export const Signals = {}

// ensures key in object
function ensure(name) {
  this[name] = this[name] || []
}

// generic signal builder
function genericSignal(name){
  return {
    hook: (...data) => each(f => f(data)),
    init: hook => window.addEventListener(name, hook),
    term: hook => window.removeEventListener(name, hook)
  }
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
  Builds a signal stream
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
export default function Stream(state) {
  // data and api
  const self = { data: {}, api: {}, state}

  // build api
  const keys = Object.keys(Signals)

  keys::each(name => {
    self.api[name] = f => {
      self.data::ensure('keydown')
      self.data.keydown.push(f)
      return self.api
    }
  })

  // output api and data
  return self
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
  Adds listeners
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
export function watch() {
  const keys = Object.keys(this.data)
  keys::each(name => {
    const event = Signals[name]
    event.init(event.hook)
  })
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
  Removes listeners
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
export function unwatch() {
  const keys = Object.keys(this.data)
  keys::each(name => {
    const event = Signals[name]
    event.term(event.hook)
  })
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
  Default Signals
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const keyboard = ['keydown', 'keyup', 'keypress']
keyboard::each(name => Signals[name] = genericSignal(name))


/**
  DELAYED: blur focus click dblclick wheel mousedown* mouseup*
  ANALYZE: focusin* focusout* mouseenter* mouseleave* mousemove*
           mouseout* mouseover* resize*
  LATER: compositionend** compositionstart** compositionupdate**
*/
