/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
  Prelude.List.flatMap
    Maps 'List this' with 'f' then flattens the result
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  This file is part of 'carrot-engine'.

  'carrot-engine' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  'carrot-engine' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with 'carrot-engine'. If not, see <http://www.gnu.org/licenses/>.
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

export default function flatMap(f){
  const out = []

  let length = this.length
    , i = 0

  for (; i < length; ++i){
    const item = f(this[i])

    if (item instanceof Array) {
      const part = item::flatten()

      let ʹlength = part.length
        , ʹi = 0

      for (; ʹi < ʹlength; ++ʹi){
        out.push(part[ʹi])
      }
    } else {
      out.push(item)
    }
  }
  return out
}
