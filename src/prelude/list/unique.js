/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
  Prelude.List.unique
    Removes duplicated properties from the list
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  This file is part of 'carrot-engine'.

  'carrot-engine' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  'carrot-engine' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with 'carrot-engine'. If not, see <http://www.gnu.org/licenses/>.
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

export default function unique(array) {
  const u = {}, a = []
  for(var i = 0, l = array.length; i < l; ++i) {
    if(u.hasOwnProperty(array[i])) {
      continue
    }
    a.push(array[i])
    u[array[i]] = 1
  }
  return a
}
