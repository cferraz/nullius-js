/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
  Prelude.List
    Utilities for List values (arrays)
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  This file is part of 'carrot-engine'.

  'carrot-engine' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  'carrot-engine' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with 'carrot-engine'. If not, see <http://www.gnu.org/licenses/>.
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// type check
export isList from '~/src/prelude/list/is-list'

// single
export each   from '~/src/prelude/list/each'
export concat from '~/src/prelude/list/concat'
export map    from '~/src/prelude/list/map'
export map$   from '~/src/prelude/list/map$'
export filter from '~/src/prelude/list/filter'
export unique from '~/src/prelude/list/unique'

// commom utilities
export indexOf from '~/src/prelude/list/index-of'
export flatMap from '~/src/prelude/list/flat-map'
export flatten from '~/src/prelude/list/flatten'
export fold    from '~/src/prelude/list/fold'
export reduce  from '~/src/prelude/list/reduce'

// alternative utilities
export indexesOf from '~/src/prelude/list/indexes-of'
export flatMap1  from '~/src/prelude/list/flat-map1'
export flatten1  from '~/src/prelude/list/flatten1'
export foldR     from '~/src/prelude/list/fold-r'
export reduceR   from '~/src/prelude/list/reduce-r'
