/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
  Prelude.List.flatten
    Flattens 'List this' until it only contains non-array values
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  This file is part of 'carrot-engine'.

  'carrot-engine' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  'carrot-engine' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with 'carrot-engine'. If not, see <http://www.gnu.org/licenses/>.
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

export default function flatten(){
  const walk = []
      , out  = []

  let curr = this
    , len  = this.length
    , idx  = 0
    , wlen = 0

  while (wlen > -1){
    let item = curr[idx]

    if (item instanceof Array) {
      walk.push(idx+1)
      wlen++
      curr = item
      len  = curr.length
      idx  = 0
    } else {
      out.push(item)
      idx++

      while (idx == len) {
        idx = walk.pop()
        wlen--
        curr = this
        for (let i = 0; i < wlen; ++i) {
          curr = curr[walk[i]-1]
        }
        len = curr.length
      }
    }
  }
  return out
}
