/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
  Prelude.Type.create
    Creates a new type class from 'src<function | [function]>' and obj<Object>
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  This file is part of 'carrot-engine'.

  'carrot-engine' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  'carrot-engine' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with 'carrot-engine'. If not, see <http://www.gnu.org/licenses/>.
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

export default function create(src, obj={}) {
  // safe guards
  src = src || [Object] // if src exists
  src = Array.isArray(src) ? src : [src] // convert to array
  src = (src.length != 0) ? src : [Object] // ensure array length

  // constructor object
  function Dummy(){}

  // no inheritance or direct inheritance
  if (src.length > 1) {
    Dummy.prototype = Object.create(Object.prototype)
  } else {
    Dummy.prototype = Object.create(src[0].prototype)
  }

  // main prototype
  Dummy.prototypes = [Dummy]

  // reversed proto, first ones are the most important
  for (let i=src.length; i >= 0; --i){
    const props = src[i]
        , proto = props.prototype

    // mix props from prototype in
    for (let key=''; key in props;) {
      if (key == 'prototypes') {
        Dummy.prototypes = Dummy.prototypes.concat(props.prototypes)
      } else if (props.hasOwnProperty(key)) {
        Dummy[key] = props[key]
      }
    }

    // add prototype props
    if (src.length > 1) {
      for (let key=''; key in proto;) {
        if (key != 'prototypes' && proto.hasOwnProperty(key)) {
          Dummy.prototype[key] = proto[key]
        }
      }
    }
  } // for loop

  for(let key=''; key in obj;) {
    if (key != 'prototype' &&
        key != 'prototypes' &&
        obj.hasOwnProperty(key)) {
      Dummy[key] = obj[key]
    }
  }

  if (obj.prototype) {
    for(let key=''; key in obj.prototype;) {
      if (key != 'prototype' &&
          key != 'prototypes' &&
          obj.prototype.hasOwnProperty(key)) {
        Dummy.prototype[key] = obj.prototype[key]
      }
    }
  }

  return Dummy
}
