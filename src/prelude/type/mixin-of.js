/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
  Prelude.Type.mixinOf
    Checks if 'this' is a mixin of 'obj'
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  This file is part of 'carrot-engine'.

  'carrot-engine' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  'carrot-engine' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with 'carrot-engine'. If not, see <http://www.gnu.org/licenses/>.
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

export default function mixinOf(obj){
  if (obj instanceof this) {
    return true
  } else if (obj.prototypes) {
    return obj.prototypes.indexOf(this) > -1
  } else {
    return false
  }
}
