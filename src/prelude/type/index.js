/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
  Prelude.Type
    A simple prototype system with a powerful proposal
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  This file is part of 'carrot-engine'.

  'carrot-engine' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  'carrot-engine' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with 'carrot-engine'. If not, see <http://www.gnu.org/licenses/>.
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

export create      from '~/src/prelude/type/create'
export instanceOf  from '~/src/prelude/type/instance-of'
export mixinOf     from '~/src/prelude/type/mixin-of'
export prototypeOf from '~/src/prelude/type/prototype-of'
export isDef       from '~/src/prelude/type/is-def'
export isErr       from '~/src/prelude/type/is-err'
export isVal       from '~/src/prelude/type/is-val'
