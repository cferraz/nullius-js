/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
  Prelude.Logic
    Useful functions for Logic values (true/false/null/undefined)
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  This file is part of 'carrot-engine'.

  'carrot-engine' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  'carrot-engine' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with 'carrot-engine'. If not, see <http://www.gnu.org/licenses/>.
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

export and  from '~/src/prelude/logic/and'
export id   from '~/src/prelude/logic/id'
export nand from '~/src/prelude/logic/nand'
export nor  from '~/src/prelude/logic/nor'
export not  from '~/src/prelude/logic/not'
export nxor from '~/src/prelude/logic/nxor'
export or   from '~/src/prelude/logic/or'
export xor  from '~/src/prelude/logic/xor'
