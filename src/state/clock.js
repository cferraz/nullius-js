/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
  State.Clock
    Clock procedure, calls the same function with a 16ms interval
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  This file is part of 'carrot-engine'.

  'carrot-engine' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  'carrot-engine' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with 'carrot-engine'. If not, see <http://www.gnu.org/licenses/>.
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Module Definition
import nextTick from '~/src/util/next-tick'
import currentTime from '~/src/util/current-time'
import timeout from '~/src/util/timeout'
export {currentTime, nextTick, timeout}

// Constants
const interval = 16.005

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *\
  Main Procedure
\* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
export default function Clock(callback) {
  var stop = false
    , time = currentTime() // "clockTick" call time

  // retuns a function that stops the process
  function stopClock() {
    stop = true
  }

  // clock tick function
  function clockTick() {
    const tickInit = currentTime()

    callback(stopClock, tickInit)

    // reset local time
    time = currentTime()

    // wait until:
    const until = time + ((interval - (time - tickInit)))

    // delay and process
    delay(until, function(delayTime) {
      if (!stop) {
        clockTick(stopClock, delayTime)
      }
    })
  }

  // first tick
  clockTick()
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *\
  Delays the execution of the function
\* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
export function delay(until, fn) {
  const target = until - currentTime()

  // just wait for io because of lag
  if (target < 0) {
    nextTick(function waitIO(){
      fn(currentTime())
    })

  // wait the whole thing
  } else {
    const padding = target-4

    // wait a little
    if (padding < 0) {
      wait(until, fn) // last 4ms

    // wait a lot
    } else {
      timeout(target, function (){
        wait(until, fn) // wait the last 4ms
      })
    }
  } // if
} // function

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *\
  Waits for given time
\* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
export function wait(time, done) {
  // recursive wait
  function nextWait(time, done) {
    if (currentTime() < time) {
      nextTick(function waiting() {
        nextWait(time, done)
      })
    } else {
      done(currentTime())
    }
  }

  // counter stackoverflow initiation block
  nextTick(function() {
    if (currentTime() < time) {
      done(currentTime())
    } else {
      nextWait(time, done)
    }
  })
}
