/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
  Game.Entity.Player
    Example entity for Player
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  This file is part of 'carrot-engine'.

  'carrot-engine' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  'carrot-engine' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with 'carrot-engine'. If not, see <http://www.gnu.org/licenses/>.
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// called when the scene starts
export default function Player() {
  // (this = impure context)
  // => Array<Entity>
}

// initialize (this = impure context)
export function initialize(msg, state) {
  // (this = impure context)
  return [] // => Function | [Function]
}

// called at each game tick
export function update(past, state) {
  // (this = impure context)
  if (state.left)  { state.x += 1 }
  if (state.right) { state.x -= 1 }
  return [] // => Function | [Function]
}

// called once the scene changes (this = impure context)
export function dispose(state, msg) {
  // (this = impure context)
  return [] // => Function | [Function]
}
