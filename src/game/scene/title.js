/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
  Game.Scene.Title
    Example scene for Title
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  This file is part of 'carrot-engine'.

  'carrot-engine' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  'carrot-engine' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with 'carrot-engine'. If not, see <http://www.gnu.org/licenses/>.
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
function noop(...args){ console.log(args) }

// called when the scene starts
export default function Title(stream) { // signal stream (this=impure context)

  this.left  = this.left  || noop
  this.right = this.right || noop
  this.up    = this.up    || noop
  this.down  = this.down  || noop

  stream
    .keydown(keycode => {
      switch(keycode){
      case 37: return this.left(true)
      case 38: return this.up(true)
      case 39: return this.right(true)
      case 40: return this.down(true)
      }
    })
    .keyup(keycode => {
      switch(keycode){
      case 37: return this.left(false)
      case 38: return this.up(false)
      case 39: return this.right(false)
      case 40: return this.down(false)
      }
    })
}

// initialize (this = impure context)
export function initialize() {
  return
}

// called at each game tick (this = impure context)
export function update() {
  return
}

// called once the scene changes
export function dispose() {
  return
}
