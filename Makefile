default: all

SRC = $(shell find src -name "*.js" -type f | sort)
LIB = $(SRC:src/%.js=lib/%.js)

BABEL = node_modules/.bin/babel
WATCH = node_modules/.bin/watch
BUNDLE = node_modules/.bin/browserify
WATCHY = node_modules/.bin/watchify
EXTERN = node_modules/.bin/exorcist
ESLINT = node_modules/.bin/eslint
MOCHA = node_modules/.bin/mocha
MOCHA2 = node_modules/.bin/_mocha
ISTANBUL = node_modules/.bin/istanbul
LICENSE = node_modules/.bin/license-checker
PACK = node_modules/.bin/electron-packager
DEBUG = node_modules/.bin/electron
IGNORE = --ignore="(node_modules/|src/|lib/|test/|concat/|\.babelrc|\.eslintrc\.js|\.gitignore|Makefile|package\.json|README\.md|\.git|COPYING|LICENSE\.txt)"
VERSION = --version-string.CompanyName="Company Inc." --version-string.ProductName="Product"
FLOW = node_modules/.bin/flow

##
# file targets
##
lib:
	mkdir -p lib/

dist:
	mkdir -p dist/

src/%.js:
	cp ./scripts/newfile.js $@

lib/%.js: src/%.js lib
	@mkdir -p $(@D)
	$(ESLINT) --cache "$<"
	$(BABEL) "$<" --out-file $@ --source-maps

dist/bundle.js: lint dist
ifeq ($(MINIFY),false)
	$(BUNDLE) -p bundle-collapser/plugin \
		-t eslintify --continuous \
		-t babelify -d src/index.js \
		-o dist/bundle.js
else
	$(BUNDLE) -p bundle-collapser/plugin \
		-p [minifyify --map dist/bundle.js.map --output dist/bundle.js --minify=] \
		-t eslintify --continuous \
		-t babelify -d src/index.js \
		-o dist/bundle.js
endif

##
# grouping rules
##

all: build

lint:
	$(ESLINT) --cache ./src ./test

build: $(LIB)

browserify: dist/bundle.js

watchify: browserify
ifeq ($(MINIFY),false)
	$(WATCHY) -p bundle-collapser/plugin \
		-t eslintify --continuous \
		-t babelify -d src/index.js \
		-o dist/bundle.js \
		-v
else
	$(WATCHY) -p bundle-collapser/plugin \
		-p [minifyify --map bundle.js.map --output dist/bundle.js.map --minify=$(MINIFY)] \
		-t eslintify --continuous \
		-t babelify -d src/index.js \
		-o dist/bundle.js \
		-v
endif

pack: browserify
	$(PACK) ./ "Game" --platform=all --arch=all --overwrite --asar $(VERSION) $(IGNORE)

pack-win: browserify
	$(PACK) ./ "Game" --platform=win32 --arch=all --overwrite --asar $(VERSION) $(IGNORE)

pack-linux: browserify
	$(PACK) ./ "Game" --platform=linux --arch=all --overwrite --asar $(VERSION) $(IGNORE)

pack-osx: browserify
	$(PACK) ./ "Game" --platform=darwin,mas --arch=x64 --overwrite --asar $(VERSION) $(IGNORE)

pack-win32: browserify
	$(PACK) ./ "Game" --platform=win32 --arch=ia32 --overwrite --asar $(VERSION) $(IGNORE)

pack-win64: browserify
	$(PACK) ./ "Game" --platform=win32 --arch=x64 --overwrite --asar $(VERSION) $(IGNORE)

pack-linux32: browserify
	$(PACK) ./ "Game" --platform=linux --arch=ia32 --overwrite --asar $(VERSION) $(IGNORE)

pack-linux64: browserify
	$(PACK) ./ "Game" --platform=linux --arch=x64 --overwrite --asar $(VERSION) $(IGNORE)

pack-darwin: browserify
	$(PACK) ./ "Game" --platform=darwin --arch=x64 --overwrite --asar $(VERSION) $(IGNORE)

pack-mas: browserify
	$(PACK) ./ "Game" --platform=mas --arch=x64 --overwrite --asar $(VERSION) $(IGNORE)

debug: browserify
	$(DEBUG) .

loc:
	wc --lines src/*

clean:
	rm -rf lib dist coverage

watch:
	$(WATCH) "make build" ./src

test: build
	$(MOCHA) --recursive --reporter dot --ui bdd --compilers js:babel-register

coverage: build
	$(ISTANBUL) cover $(MOCHA2) -- --reporter dot --ui bdd --compilers js:babel-register

license:
	$(LICENSE) --production

##
# special rules
##

.PHONY: build loc clean watch browserify watchify lint coverage license debug pack pack-win pack-win32 pack-win64 pack-linux pack-linux32 pack-linux64 pack-osx pack-darwin pack-mas flow typecheck

.SECONDARY: $(LIB)
