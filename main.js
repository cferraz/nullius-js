/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Electron Entry Point (es5, effectful)
 * Main electron script.
 * ~ ~~ ~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~~ ~~ ~~ ~
 * This file is part of 'carrot-engine'.
 *
 * 'carrot-engine' is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 'carrot-engine' is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with 'carrot-engine'.  If not, see <http://www.gnu.org/licenses/>.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

'use strict';

require('electron-debug')({showDevTools: true});

const electron = require('electron')
    , app = electron.app // application life
    , BrowserWindow = electron.BrowserWindow; // create native browser window

var win = null;

app.on('ready', function onReady() {
  win = new BrowserWindow({
    width: 800,
    height: 600,
    frame: false,
    fullscreen: true
  });

  win.loadURL('file://' + __dirname + '/index.html');
  win.webContents.openDevTools(); // Open the DevTools.

  win.on('closed', function() {
    win = null;
  });
});

app.on('window-all-closed', function() {
  if (process.platform != 'darwin') {
    app.quit();
  }
});
