![Alt text](/res/logo.png?raw=true "Carrot Engine")

# carrot-engine

MMO farming game made with [stainless-js](https://github.com/stainless-js/stainless).

## Contributing

Do not send pull requests, this project might change it's license at any point
so it's better to not receive external contributions.

Use the issue tracker to report errors.

## License

    Copyright (C) 2016, Christian Ferraz Lemos de Sousa <cferraz95@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
